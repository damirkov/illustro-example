import 'package:illustro/products/models/product.dart';

class ProductsRepository {
  Future<List<Product>> getProducts() async {
    List<Product> _products = [];
    _products.add(
      Product(
        id: '1234',
        productName: 'product1',
        productCategories: ['ananas', 'kruska', 'marelica'],
      ),
    );
    _products.add(
      Product(
        id: '2345',
        productName: 'product2',
        productCategories: ['ananas', 'kruska', 'marelica'],
      ),
    );
    _products.add(
      Product(
        id: '3345',
        productName: 'product3',
        productCategories: ['ananas', 'kruska', 'marelica'],
      ),
    );
    _products.add(
      Product(
        id: '4234',
        productName: 'product4',
        productCategories: ['ananas', 'kruska', 'marelica'],
      ),
    );
    await Future.delayed(Duration(seconds: 10));
    return _products;
  }
}
