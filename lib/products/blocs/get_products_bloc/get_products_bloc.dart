import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:illustro/products/models/product.dart';
import 'package:illustro/products/resources/products_repository.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

part 'get_products_event.dart';
part 'get_products_state.dart';

class GetProductsBloc extends Bloc<GetProductsEvent, GetProductsState> {
  GetProductsBloc() : super(GetProductsInitial());

  //obicno repositorije se stavi na razinu cijele aplikacije
  ProductsRepository _productsRepository = ProductsRepository();

  @override
  Stream<GetProductsState> mapEventToState(
    GetProductsEvent event,
  ) async* {
    if (event is GetProductsGetEvent) {
      yield GetProductsLoading();
      try {
        List<Product> _products = await _productsRepository.getProducts();
        yield GetProductsSuccess(_products);
      } catch (e) {
        yield GetProductsError(e.toString());
      }
    }
  }
}
