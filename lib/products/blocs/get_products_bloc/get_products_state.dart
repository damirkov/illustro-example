part of 'get_products_bloc.dart';

@immutable
abstract class GetProductsState extends Equatable {
  @override
  List<Object?> get props => [];
}

class GetProductsInitial extends GetProductsState {}

class GetProductsLoading extends GetProductsState {}

class GetProductsError extends GetProductsState {
  final String message;

  GetProductsError(this.message);

  @override
  List<Object?> get props => [message];
}

class GetProductsSuccess extends GetProductsState {
  final List<Product> products;

  GetProductsSuccess(this.products);

  @override
  List<Object?> get props => [products];
}
