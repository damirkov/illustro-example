part of 'get_products_bloc.dart';

@immutable
abstract class GetProductsEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class GetProductsGetEvent extends GetProductsEvent {}
