import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:illustro/products/blocs/get_products_bloc/get_products_bloc.dart';
import 'package:illustro/products/models/product.dart';
import 'package:rxdart/rxdart.dart';

part 'scan_products_event.dart';
part 'scan_products_state.dart';

class ScanProductsBloc extends Bloc<ScanProductsEvent, ScanProductsState> {
  ScanProductsBloc(this.getProductsBloc) : super(ScanProductsSuccess([], null));

  final GetProductsBloc getProductsBloc;

  @override
  Stream<Transition<ScanProductsEvent, ScanProductsState>> transformEvents(
    Stream<ScanProductsEvent> events,
    Stream<Transition<ScanProductsEvent, ScanProductsState>> Function(
      ScanProductsEvent event,
    )
        transitionFn,
  ) {
    return events
        .debounceTime(const Duration(milliseconds: 100))
        .switchMap(transitionFn);
  }

  @override
  Stream<ScanProductsState> mapEventToState(
    ScanProductsEvent event,
  ) async* {
    if (event is ScanProductScanned &&
        getProductsBloc.state is GetProductsSuccess) {
      List<Product> _products =
          (getProductsBloc.state as GetProductsSuccess).products;
      try {
        Product _findProduct =
            _products.firstWhere((product) => product.id == event.code);
        if (!state.products.contains(_findProduct)) {
          List<Product> _newState = List.from(state.products)
            ..add(_findProduct);
          yield ScanProductsSuccess(_newState, _findProduct);
        }
      } catch (e) {
        yield ScanProductsError(
            state.products, 'Product (${event.code}) not found!');
      }
    }

    if (event is ScanProductRemoved) {
      List<Product> _newState = List.from(state.products)
        ..remove(event.product);
      yield ScanProductsSuccess(_newState, null);
    }
  }
}
