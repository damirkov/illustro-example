part of 'scan_products_bloc.dart';

abstract class ScanProductsState extends Equatable {
  const ScanProductsState();

  @override
  List<Object?> get props => [];

  List<Product> get products => [];
}

class ScanProductsInitial extends ScanProductsState {}

class ScanProductsSuccess extends ScanProductsState {
  final List<Product> products;
  final Product? scanned;

  ScanProductsSuccess(this.products, this.scanned);

  @override
  List<Object?> get props => [products, scanned];
}

class ScanProductsError extends ScanProductsState {
  final List<Product> products;
  final String message;

  ScanProductsError(this.products, this.message);

  @override
  List<Object?> get props => [products, message];
}
