part of 'scan_products_bloc.dart';

abstract class ScanProductsEvent extends Equatable {
  const ScanProductsEvent();

  @override
  List<Object?> get props => [];
}

class ScanProductScanned extends ScanProductsEvent {
  final String code;

  ScanProductScanned(this.code);

  @override
  List<Object?> get props => [code];
}

class ScanProductRemoved extends ScanProductsEvent {
  final Product product;

  ScanProductRemoved(this.product);

  @override
  List<Object?> get props => [product];
}
