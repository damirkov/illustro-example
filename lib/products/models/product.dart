class Product {
  final String id;
  final String productName;
  final List<String> productCategories;

  Product({
    required this.id,
    required this.productName,
    required this.productCategories,
  });
}
