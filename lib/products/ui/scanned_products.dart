import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:illustro/products/blocs/scan_products_bloc/scan_products_bloc.dart';
import 'package:illustro/products/models/product.dart';
import 'package:illustro/products/ui/scanner.dart';

class ScannedProductsPage extends StatefulWidget {
  final ScanProductsBloc scanProductsBloc;

  ScannedProductsPage(this.scanProductsBloc, {Key? key}) : super(key: key);

  @override
  _ScannedProductsPageState createState() => _ScannedProductsPageState();
}

class _ScannedProductsPageState extends State<ScannedProductsPage> {
  void _scanProducts() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return ScannerView(widget.scanProductsBloc);
      }),
    );
  }

  void _deleteProduct(Product product) {
    widget.scanProductsBloc.add(ScanProductRemoved(product));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScanProductsBloc, ScanProductsState>(
      bloc: widget.scanProductsBloc,
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text('Scanned products'),
          ),
          body: ListView.builder(
            itemCount: state.products.length,
            padding: const EdgeInsets.symmetric(vertical: 16),
            itemBuilder: (BuildContext context, int index) {
              Product _product = state.products[index];
              return ListTile(
                leading: Text(_product.id),
                title: Text(_product.productName),
                subtitle: Text(
                  _product.productCategories.toString(),
                ),
                trailing: IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () => _deleteProduct(_product),
                ),
              );
            },
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: _scanProducts,
            tooltip: 'Scan products',
            child: Icon(Icons.scanner),
          ),
        );
      },
    );
  }
}
