import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:illustro/products/blocs/get_products_bloc/get_products_bloc.dart';
import 'package:illustro/products/blocs/scan_products_bloc/scan_products_bloc.dart';
import 'package:illustro/products/ui/scanned_products.dart';
import 'package:illustro/products/ui/scanner.dart';

class ProductsPage extends StatefulWidget {
  ProductsPage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _ProductsPageState createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage> {
  final GetProductsBloc _getProductsBloc = GetProductsBloc()
    ..add(GetProductsGetEvent());
  late ScanProductsBloc _scanProductsBloc;

  @override
  void initState() {
    _scanProductsBloc = ScanProductsBloc(_getProductsBloc);
    super.initState();
  }

  void _scanProducts() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return ScannedProductsPage(_scanProductsBloc);
      }),
    );
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        return ScannerView(_scanProductsBloc);
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => _getProductsBloc,
        ),
        BlocProvider(
          create: (context) => _scanProductsBloc,
        ),
      ],
      child: BlocBuilder<GetProductsBloc, GetProductsState>(
        builder: (context, state) {
          if (state is GetProductsLoading || state is GetProductsInitial) {
            return Scaffold(
                appBar: AppBar(
                  title: Text(widget.title! + ' - Loading'),
                ),
                body: Center(
                  child: CircularProgressIndicator(),
                ));
          }
          if (state is GetProductsError) {
            return Scaffold(
                appBar: AppBar(
                  title: Text(widget.title! + ' - Error'),
                ),
                body: Center(
                  child: Text(state.message),
                ));
          }
          if (state is GetProductsSuccess) {
            return Scaffold(
              appBar: AppBar(
                title: Text(widget.title!),
              ),
              body: ListView(
                children: state.products.map((product) {
                  return ListTile(
                    leading: Text(product.id),
                    title: Text(product.productName),
                    subtitle: Text(product.productCategories.toString()),
                  );
                }).toList(),
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: _scanProducts,
                tooltip: 'Scan products',
                child: Icon(Icons.scanner),
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}
